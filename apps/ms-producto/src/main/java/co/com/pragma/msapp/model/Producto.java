/**
 * 
 */
package co.com.pragma.msapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import lombok.Data;

/**
 * @author gustavo.rodriguez
 *
 */
@Data
@Entity
@Table(name = "producto")
@NamedQuery(name="Producto.findByCodigo", query = "Select p from Producto p where p.proCodigo = ?1")
public class Producto {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pet_generator")
	@SequenceGenerator(name="pet_generator", sequenceName = "pet_seq")
	private int proId;
	private String proCodigo;
	private String proNombre;
	private double proValor;
	
	public Producto() {
		// TODO Auto-generated constructor stub
	}	
}
