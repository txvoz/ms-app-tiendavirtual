/**
 * 
 */
package co.com.pragma.msapp.bussines.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.pragma.msapp.bussines.repository.ProductoRepository;
import co.com.pragma.msapp.model.Producto;

/**
 * @author gustavo.rodriguez
 *
 */
@Service
@Transactional(readOnly = true)
public class ProductoService {
	private final ProductoRepository productoRepository;
	
	public ProductoService(ProductoRepository productoRepository) {
		this.productoRepository = productoRepository;
	}
	
	/**
	 * Metodo para crear un producto
	 * @param producto
	 * @return
	 */
	@Transactional
	public Producto create(Producto producto) {
		return this.productoRepository.save(producto);
	}
	
	/**
	 * Metodo para actualizar un producto
	 * @param producto
	 * @return
	 */
	@Transactional
	public Producto update(Producto producto) {
		return this.productoRepository.save(producto);
	}
	
	/**
	 * Metodo para eliminar un producto
	 * @param producto
	 */
	@Transactional
	public void delete(Producto producto) {
		this.productoRepository.delete(producto);
	}
	
	/**
	 * Metodo para buscar un producto por codigo
	 * @param proCodigo
	 * @return
	 */
	public Producto findByProCodigo(String codigo) {
		return this.productoRepository.findByCodigo(codigo);
	}
	
	/**
	 * Metodo para traer todos los registros de productos
	 * @return
	 */
	public List<Producto> findAll(){
		return this.productoRepository.findAll();
	}
}
