/**

 * 
 */
package co.com.pragma.msapp.views.resources;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.pragma.msapp.bussines.services.ProductoService;
import co.com.pragma.msapp.model.Producto;
import co.com.pragma.msapp.views.resources.vo.ProductoVO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase para la exposicion de los servicios web del cliente
 * @author gustavo.rodriguez
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/api/producto")
public class ProductoResource {
	
	private final ProductoService productoService;
	
	public ProductoResource(ProductoService productoService) {
		this.productoService = productoService;
	}
	
	@PostMapping
	@ApiOperation( value = "Crear producto", notes = "Servicio para crear un nuevo producto")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Producto creado correctamente"),
			@ApiResponse(code=400, message = "Solicitud no valida")
	})
	public ResponseEntity<Producto> create(@RequestBody ProductoVO productoVO){
		Producto producto = new Producto();
		producto.setProCodigo(productoVO.getProCodigo());
		producto.setProNombre(productoVO.getProNombre());
		producto.setProValor(productoVO.getProValor());
		return new ResponseEntity<>(this.productoService.create(producto),HttpStatus.CREATED);
	}
	
	@PutMapping("/{codigo}")
	@ApiOperation( value = "Actualizar producto", notes = "Servicio para actualizar un producto")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Producto actualizado correctamente"),
			@ApiResponse(code=404, message = "Producto no encontrado")
	})
	public ResponseEntity<Producto> update(@PathVariable("codigo") String codigo,@RequestBody ProductoVO productoVO){
		Producto producto = this.productoService.findByProCodigo(codigo);
		if(producto==null) {
			return new ResponseEntity<Producto>(HttpStatus.NOT_FOUND);
		}
		producto.setProCodigo(productoVO.getProCodigo());
		producto.setProNombre(productoVO.getProNombre());
		producto.setProValor(productoVO.getProValor());
		return new ResponseEntity<>(this.productoService.update(producto),HttpStatus.OK);
	}
	
	@DeleteMapping("/{codigo}")
	@ApiOperation( value = "Eliminar producto", notes = "Servicio para eliminar un producto")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Producto eliminado correctamente"),
			@ApiResponse(code=404, message = "Producto no encontrado")
	})
	public ResponseEntity<Producto> remove(@PathVariable("codigo") String codigo){
		Producto producto = this.productoService.findByProCodigo(codigo);
		if(producto!=null) {
			this.productoService.delete(producto);
		}
		return new ResponseEntity<Producto>(HttpStatus.NOT_FOUND);
	}
	
	@GetMapping
	@ApiOperation( value = "Listar productos", notes = "Servicio para listar todos los productos")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Productos encontrados"),
			@ApiResponse(code=404, message = "Productos no encontrados")
	})
	public ResponseEntity<List<Producto>> findAll(){
		return ResponseEntity.ok(this.productoService.findAll());
	}
	
	@GetMapping("/{codigo}")
	@ApiOperation( value = "Traer producto", notes = "Servicio para traer el detalle de un producto por codigo")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Producto encontrado"),
			@ApiResponse(code=404, message = "Producto no encontrado")
	})
	public ResponseEntity<Producto> findByIdentificacion(@PathVariable("codigo") String codigo){
		Producto producto= this.productoService.findByProCodigo(codigo);
		if(producto!=null) {
			return ResponseEntity.ok(producto);
		}
		return new ResponseEntity<Producto>(HttpStatus.NOT_FOUND);
	}
	
}
