/**
 * 
 */
package co.com.pragma.msapp.views.resources.vo;

import lombok.Data;

/**
 * @author gustavo.rodriguez
 *
 */
@Data
public class ProductoVO {
	private int proId;
	private String proCodigo;
	private String proNombre;
	private double proValor;
	
	public ProductoVO() {}
}
