/**
 * 
 */
package co.com.pragma.msapp.bussines.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.msapp.model.Producto;

/**
 * @author gustavo.rodriguez
 *
 */
public interface ProductoRepository extends JpaRepository<Producto, String> {
	
	/**
	 * Definicion de metodo para buscar productos por el valor del nombre del producto
	 * @param proNombre
	 * @return
	 */
	public List<Producto> findByProNombre(String proNombre);
	
	/**
	 * Definicion de metodo para buscar producto por el valor del codigo de producto
	 * @param proCodigo
	 * @return
	 */
	public Producto findByCodigo(String proCodigo);
}
