/**
 * 
 */
package co.com.pragma.msapp.bussines.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.msapp.model.Venta;

/**
 * @author gustavo.rodriguez
 *
 */
public interface VentaRepository extends JpaRepository<Venta, String> {
	
	/**
	 * Metodo para traer las ventas por medio de un id de venta 
	 * @param venId
	 * @return
	 */
	public Venta findByVenId(int venId);
	
	/**
	 * Metodo para traer las ventas de un cliente 
	 * @param cliId
	 * @return
	 */
	public List<Venta> findByCliId(int cliId);
	
	/**
	 * Metodo para traer las ventas por medio de una fecha
	 * @param venFecha
	 * @return
	 */
	public List<Venta> findByFecha(Date venFecha);
	
	/**
	 * Metodo para traer las compras de un cliente filtrando por la fecha 
	 * @param cliId
	 * @param venFecha
	 * @return
	 */
	public List<Venta> findByCliIdAndVenFecha(int cliId, Date venFecha);
}
