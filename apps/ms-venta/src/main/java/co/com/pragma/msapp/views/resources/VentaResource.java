/**

 * 
 */
package co.com.pragma.msapp.views.resources;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.pragma.msapp.bussines.services.VentaService;
import co.com.pragma.msapp.model.Venta;
import co.com.pragma.msapp.views.resources.vo.VentaVO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase para la exposicion de los servicios web del cliente
 * @author gustavo.rodriguez
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/api/venta")
public class VentaResource {
	
	private final VentaService ventaService;
	
	public VentaResource(VentaService ventaService) {
		this.ventaService = ventaService;
	}
	
	@PostMapping
	@ApiOperation( value = "Crear venta", notes = "Servicio para crear una nueva venta")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Venta creado correctamente"),
			@ApiResponse(code=400, message = "Solicitud no valida")
	})
	public ResponseEntity<Venta> create(@RequestBody VentaVO ventaVO){
		Venta venta = new Venta();
		venta.setVenFecha(ventaVO.getVenFecha());
		venta.setVenValor(ventaVO.getVenValor());
		venta.setCliId(ventaVO.getCliId());
		return new ResponseEntity<>(this.ventaService.create(venta),HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	@ApiOperation( value = "Actualizar venta", notes = "Servicio para actualizar una venta")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Venta actualizado correctamente"),
			@ApiResponse(code=404, message = "Venta no encontrada")
	})
	public ResponseEntity<Venta> update(@PathVariable("id") int id,@RequestBody VentaVO ventaVO){
		Venta venta = this.ventaService.findByVenId(id);
		if(venta==null) {
			return new ResponseEntity<Venta>(HttpStatus.NOT_FOUND);
		}
		venta.setVenFecha(ventaVO.getVenFecha());
		venta.setVenValor(ventaVO.getVenValor());
		venta.setCliId(ventaVO.getCliId());
		return new ResponseEntity<>(this.ventaService.update(venta),HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation( value = "Eliminar venta", notes = "Servicio para eliminar un venta")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Venta eliminado correctamente"),
			@ApiResponse(code=404, message = "Venta no encontrado")
	})
	public ResponseEntity<Venta> remove(@PathVariable("id") int id){
		Venta venta = this.ventaService.findByVenId(id);
		if(venta!=null) {
			this.ventaService.delete(venta);
		}
		return new ResponseEntity<Venta>(HttpStatus.NOT_FOUND);
	}
	
	@GetMapping
	@ApiOperation( value = "Listar ventas", notes = "Servicio para listar todos las ventas")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Ventas encontrados"),
			@ApiResponse(code=404, message = "Ventas no encontrados")
	})
	public ResponseEntity<List<Venta>> findAll(){
		return ResponseEntity.ok(this.ventaService.findAll());
	}
	
	@GetMapping("/{id}")
	@ApiOperation( value = "Traer venta", notes = "Servicio para traer el detalle de un venta por codigo")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Venta encontrado"),
			@ApiResponse(code=404, message = "Venta no encontrado")
	})
	public ResponseEntity<Venta> findByVenId(@PathVariable("id") int id){
		Venta venta= this.ventaService.findByVenId(id);
		if(venta!=null) {
			return ResponseEntity.ok(venta);
		}
		return new ResponseEntity<Venta>(HttpStatus.NOT_FOUND);
	}
	
}
