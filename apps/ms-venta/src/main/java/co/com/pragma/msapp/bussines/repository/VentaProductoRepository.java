/**
 * 
 */
package co.com.pragma.msapp.bussines.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.msapp.model.Venta;
import co.com.pragma.msapp.model.VentaProducto;

/**
 * @author gustavo.rodriguez
 *
 */
public interface VentaProductoRepository extends JpaRepository<VentaProducto, String> {
	
	/**
	 * Metodo para traer un producto por id de la venta producto
	 * @param vpId
	 * @return
	 */
	public VentaProducto findByVpId(int vpId);
	
	/**
	 * Metodo para traer una lista de productos que estan asociados a una venta por 
	 * medio de el id de la venta 
	 * @param venID
	 * @return
	 */
	public List<VentaProducto> findByVentaVenId(int venId);
	
}
