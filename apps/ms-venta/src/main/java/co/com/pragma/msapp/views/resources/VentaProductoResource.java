/**

 * 
 */
package co.com.pragma.msapp.views.resources;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.pragma.msapp.bussines.services.VentaProductoService;
import co.com.pragma.msapp.model.Producto;
import co.com.pragma.msapp.model.Venta;
import co.com.pragma.msapp.model.VentaProducto;
import co.com.pragma.msapp.views.resources.vo.VentaProductoVO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase para la exposicion de los servicios web del cliente
 * @author gustavo.rodriguez
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/api/ventaproducto")
public class VentaProductoResource {
	
	private final VentaProductoService ventaProductoService;
	
	public VentaProductoResource(VentaProductoService ventaService) {
		this.ventaProductoService = ventaService;
	}
	
	@PostMapping
	@ApiOperation( value = "Agregar producto a venta", notes = "Servicio para crear una nueva venta producto")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Producto asociadocorrectamente"),
			@ApiResponse(code=400, message = "Solicitud no valida")
	})
	public ResponseEntity<VentaProducto> create(@RequestBody VentaProductoVO vpVO){
		VentaProducto vP = new VentaProducto();
		vP.setVpCantidad(vpVO.getVpCantidad());
		vP.setVpPrecioUnidad(vpVO.getVpPrecioUnidad());
		//*******
		Producto p = new Producto();
		p.setProId(vpVO.getProId());
		vP.setProducto(p);
		//*******
		Venta v = new Venta();
		v.setVenId(vpVO.getVenId());
		vP.setVenta(v);
		//*******
		return new ResponseEntity<>(this.ventaProductoService.create(vP),HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	@ApiOperation( value = "Actualizar venta", notes = "Servicio para actualizar una venta")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Venta actualizado correctamente"),
			@ApiResponse(code=404, message = "Venta no encontrada")
	})
	public ResponseEntity<VentaProducto> update(@PathVariable("id") int id,@RequestBody VentaProductoVO vpVO){
		VentaProducto ventaProducto = this.ventaProductoService.findByVpId(id);
		if(ventaProducto==null) {
			return new ResponseEntity<VentaProducto>(HttpStatus.NOT_FOUND);
		}
		VentaProducto vP = new VentaProducto();
		vP.setVpCantidad(vpVO.getVpCantidad());
		vP.setVpPrecioUnidad(vpVO.getVpPrecioUnidad());
		//*******
		Producto p = new Producto();
		p.setProId(vpVO.getProId());
		vP.setProducto(p);
		//*******
		Venta v = new Venta();
		v.setVenId(vpVO.getVenId());
		vP.setVenta(v);
		//*******
		return new ResponseEntity<>(this.ventaProductoService.update(vP),HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation( value = "Eliminar producto de venta", notes = "Servicio para eliminar un producto de venta")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Venta eliminado correctamente"),
			@ApiResponse(code=404, message = "Venta no encontrado")
	})
	public ResponseEntity<VentaProducto> remove(@PathVariable("id") int id){
		VentaProducto vp = this.ventaProductoService.findByVpId(id);
		if(vp!=null) {
			this.ventaProductoService.delete(vp);
		}
		return new ResponseEntity<VentaProducto>(HttpStatus.NOT_FOUND);
	}
	
	@GetMapping
	@ApiOperation( value = "Listar ventas", notes = "Servicio para listar todos los productos de una venta")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Ventas encontrados"),
			@ApiResponse(code=404, message = "Ventas no encontrados")
	})
	public ResponseEntity<List<VentaProducto>> findAll(){
		return ResponseEntity.ok(this.ventaProductoService.findAll());
	}
	
	@GetMapping("/{id}")
	@ApiOperation( value = "Traer producto venta", notes = "Servicio para traer el detalle de un producto venta por codigo")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Venta encontrado"),
			@ApiResponse(code=404, message = "Venta no encontrado")
	})
	public ResponseEntity<VentaProducto> findByVpId(@PathVariable("id") int id){
		VentaProducto vp = this.ventaProductoService.findByVpId(id);
		if(vp!=null) {
			return ResponseEntity.ok(vp);
		}
		return new ResponseEntity<VentaProducto>(HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/byventa/{id}")
	@ApiOperation( value = "Listar ventas", notes = "Servicio para listar todos los productos de una venta")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Ventas encontrados"),
			@ApiResponse(code=404, message = "Ventas no encontrados")
	})
	public ResponseEntity<List<VentaProducto>> findByVenId(@PathVariable("id") int id){
		return ResponseEntity.ok(this.ventaProductoService.findByVenId(id));
	}
	
}
