/**
 * 
 */
package co.com.pragma.msapp.bussines.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.pragma.msapp.bussines.repository.VentaProductoRepository;
import co.com.pragma.msapp.model.Venta;
import co.com.pragma.msapp.model.VentaProducto;

/**
 * @author gustavo.rodriguez
 *
 */
@Service
@Transactional(readOnly = true)
public class VentaProductoService {
	private final VentaProductoRepository ventaProductoRepository;
	
	public VentaProductoService(VentaProductoRepository ventaRepository) {
		this.ventaProductoRepository = ventaRepository;
	}
	
	/**
	 * Metodo para crear un producto
	 * @param venta
	 * @return
	 */
	@Transactional
	public VentaProducto create(VentaProducto ventaProducto) {
		return this.ventaProductoRepository.save(ventaProducto);
	}
	
	/**
	 * Metodo para actualizar un producto
	 * @param venta
	 * @return
	 */
	@Transactional
	public VentaProducto update(VentaProducto ventaProducto) {
		return this.ventaProductoRepository.save(ventaProducto);
	}
	
	/**
	 * Metodo para eliminar un producto
	 * @param venta
	 */
	@Transactional
	public void delete(VentaProducto ventaProducto) {
		this.ventaProductoRepository.delete(ventaProducto);
	}
	
	/**
	 * Metodo para buscar un producto por codigo
	 * @param proCodigo
	 * @return
	 */
	public VentaProducto findByVpId(int id) {
		return this.ventaProductoRepository.findByVpId(id);
	}
	
	/**
	 * Metodo para traer todos los registros de productos
	 * @return
	 */
	public List<VentaProducto> findAll(){
		return this.ventaProductoRepository.findAll();
	}
	
	public List<VentaProducto> findByVenId(int venId){
		Venta venta = new Venta();
		venta.setVenId(venId);
		return this.ventaProductoRepository.findByVentaVenId(venId);
	}
}
