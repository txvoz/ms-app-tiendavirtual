/**
 * 
 */
package co.com.pragma.msapp.views.resources.vo;

import java.util.Date;

import lombok.Data;

/**
 * @author gustavo.rodriguez
 *
 */
@Data
public class VentaVO {
	private int venId;
	private Date venFecha;
	private double venValor;
	private int cliId;
	
	public VentaVO() {}
}
