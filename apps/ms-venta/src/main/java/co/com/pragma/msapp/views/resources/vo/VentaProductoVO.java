/**
 * 
 */
package co.com.pragma.msapp.views.resources.vo;

import java.util.Date;

import lombok.Data;

/**
 * @author gustavo.rodriguez
 *
 */
@Data
public class VentaProductoVO {
	private int vpId;
	private int vpCantidad;
	private double vpPrecioUnidad;
	private int proId;
	private int venId;
	
	public VentaProductoVO() {}
}
