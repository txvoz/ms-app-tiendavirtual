/**
 * 
 */
package co.com.pragma.msapp.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * @author gustavo.rodriguez
 *
 */
@Data
@Entity
@Table(name = "venta_producto")
//@NamedQuery(name="venta_producto.findByVentaId", query = "Select vp from VentaProducto vp where vp.venta.venId = ?1")
public class VentaProducto {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pet_generator")
	@SequenceGenerator(name="pet_generator", sequenceName = "pet_seq")
	private int vpId;
	private double vpPrecioUnidad;
	private double vpCantidad;
	
	@ManyToOne
	@JoinColumn(name = "ven_id")
	@JsonIgnore
	private Venta venta;
	
	@ManyToOne
	@JoinColumn(name = "pro_id")
	private Producto producto;
	
	public VentaProducto() {
		// TODO Auto-generated constructor stub
	}	
}
