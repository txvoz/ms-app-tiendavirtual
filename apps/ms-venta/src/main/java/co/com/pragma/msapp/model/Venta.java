/**
 * 
 */
package co.com.pragma.msapp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author gustavo.rodriguez
 *
 */
@Data
@Entity
@Table(name = "venta")
@NamedQuery(name="Venta.findByFecha", query = "Select v from Venta v where v.venFecha = ?1")
public class Venta {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pet_generator")
	@SequenceGenerator(name="pet_generator", sequenceName = "pet_seq")
	private int venId;
	private Date venFecha;
	private double venValor;
	private int cliId;
	
	@OneToMany(mappedBy = "venta")
	private List<VentaProducto> ventaProductos;
	
	public Venta() {
		// TODO Auto-generated constructor stub
	}	
}
