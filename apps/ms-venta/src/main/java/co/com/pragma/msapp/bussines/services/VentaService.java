/**
 * 
 */
package co.com.pragma.msapp.bussines.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.pragma.msapp.bussines.repository.VentaRepository;
import co.com.pragma.msapp.model.Venta;

/**
 * @author gustavo.rodriguez
 *
 */
@Service
@Transactional(readOnly = true)
public class VentaService {
	private final VentaRepository ventaRepository;
	
	public VentaService(VentaRepository ventaRepository) {
		this.ventaRepository = ventaRepository;
	}
	
	/**
	 * Metodo para crear un producto
	 * @param venta
	 * @return
	 */
	@Transactional
	public Venta create(Venta venta) {
		return this.ventaRepository.save(venta);
	}
	
	/**
	 * Metodo para actualizar un producto
	 * @param venta
	 * @return
	 */
	@Transactional
	public Venta update(Venta venta) {
		return this.ventaRepository.save(venta);
	}
	
	/**
	 * Metodo para eliminar un producto
	 * @param venta
	 */
	@Transactional
	public void delete(Venta venta) {
		this.ventaRepository.delete(venta);
	}
	
	/**
	 * Metodo para buscar un producto por codigo
	 * @param proCodigo
	 * @return
	 */
	public Venta findByVenId(int id) {
		return this.ventaRepository.findByVenId(id);
	}
	
	/**
	 * Metodo para traer todos los registros de productos
	 * @return
	 */
	public List<Venta> findAll(){
		return this.ventaRepository.findAll();
	}
}
