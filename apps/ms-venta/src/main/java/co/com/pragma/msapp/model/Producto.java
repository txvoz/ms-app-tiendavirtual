/**
 * 
 */
package co.com.pragma.msapp.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * @author gustavo.rodriguez
 *
 */
@Data
@Entity
@Table(name = "producto")
//@NamedQuery(name="Producto.findByCodigo", query = "Select p from Producto p where p.proCodigo = ?1")
public class Producto {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pet_generator")
	@SequenceGenerator(name="pet_generator", sequenceName = "pet_seq")
	private int proId;
	private String proCodigo;
	private String proNombre;
	private double proValor;
	
	@OneToMany(mappedBy = "producto")
	@JsonIgnore
	private List<VentaProducto> ventaProductos;
	
	public Producto() {
		// TODO Auto-generated constructor stub
	}	
}
