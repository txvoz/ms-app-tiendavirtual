/**
 * 
 */
package co.com.pragma.msapp.bussines.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.pragma.msapp.bussines.repository.ClienteRepository;
import co.com.pragma.msapp.model.Cliente;

/**
 * @author gustavo.rodriguez
 *
 */
@Service
@Transactional(readOnly = true)
public class ClienteService {
	private final ClienteRepository clienteRepository;
	
	public ClienteService(ClienteRepository clienteRepository) {
		this.clienteRepository = clienteRepository;
	}
	
	/**
	 * Metodo para crear un cliente
	 * @param cliente
	 * @return
	 */
	@Transactional
	public Cliente create(Cliente cliente) {
		return this.clienteRepository.save(cliente);
	}
	
	/**
	 * Metodo para actualizar un cliente
	 * @param cliente
	 * @return
	 */
	@Transactional
	public Cliente update(Cliente cliente) {
		return this.clienteRepository.save(cliente);
	}
	
	/**
	 * Metodo para eliminar un cliente
	 * @param cliente
	 */
	@Transactional
	public void delete(Cliente cliente) {
		this.clienteRepository.delete(cliente);
	}
	
	/**
	 * Metodo para buscar un cliente por identificacion
	 * @param cliIdentificacion
	 * @return
	 */
	public Cliente findByIdentificacion(String cliIdentificacion) {
		return this.clienteRepository.findByIdentificacion(cliIdentificacion);
	}
	
	/**
	 * Metodo para traer todos los registros de cliente
	 * @return
	 */
	public List<Cliente> findAll(){
		return this.clienteRepository.findAll();
	}
}
