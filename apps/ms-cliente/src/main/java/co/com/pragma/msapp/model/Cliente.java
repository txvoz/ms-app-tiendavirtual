/**
 * 
 */
package co.com.pragma.msapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import lombok.Data;

/**
 * @author gustavo.rodriguez
 *
 */
@Data
@Entity
@Table(name = "cliente")
@NamedQuery(name="Cliente.findByIdentificacion", query = "Select c from Cliente c where c.cliIdentificacion = ?1")
public class Cliente {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pet_generator")
	@SequenceGenerator(name="pet_generator", sequenceName = "pet_seq")
	private int cliId;
	private String cliTipoIdentificacion;
	private String cliIdentificacion;
	private String cliNombres;
	private String cliApellidos;
	private char cliGenero;
	private String cliEmail;
	
	public Cliente() {
		// TODO Auto-generated constructor stub
	}	
}
