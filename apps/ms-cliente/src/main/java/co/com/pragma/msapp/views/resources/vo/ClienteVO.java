/**
 * 
 */
package co.com.pragma.msapp.views.resources.vo;

import lombok.Data;

/**
 * @author gustavo.rodriguez
 *
 */
@Data
public class ClienteVO {
	private int cliId;
	private String cliTipoIdentificacion;
	private String cliIdentificacion;
	private String cliNombres;
	private String cliApellidos;
	private char cliGenero;
	private String cliEmail;
	
	public ClienteVO() {}
}
