package co.com.pragma.reactive.reactiveapp;

import lombok.Data;

@Data
public class Cliente {

	private String id;

	private String nombres;

	private String apellidos;

	private Integer telefono;

	private String direccion;
	
	public Cliente() {}

	public Cliente(String id, String nombres, String apellidos, Integer telefono, String direccion) {
		this.id = id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.telefono = telefono;
		this.direccion = direccion;
	}

	@Override
	public String toString() {
		return "Cliente{" + "id='" + id + '\'' + ", nombres='" + nombres + '\'' + ", apellidos='" + apellidos + '\''
				+ ", telefono=" + telefono + ", direccion='" + direccion + '\'' + '}';
	}
}
